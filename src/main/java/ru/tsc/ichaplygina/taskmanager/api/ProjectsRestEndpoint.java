package ru.tsc.ichaplygina.taskmanager.api;

import org.jetbrains.annotations.Nullable;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import ru.tsc.ichaplygina.taskmanager.model.Project;

import java.util.List;

public interface ProjectsRestEndpoint {

    @Nullable
    @PostMapping("/add")
    List<Project> add(@RequestBody List<Project> projects);

    @Nullable
    @GetMapping("/findAll")
    List<Project> findAll();

    @DeleteMapping("/remove")
    void remove(@RequestBody List<Project> projects);

    @DeleteMapping("/removeAll")
    void removeAll();

    @Nullable
    @PostMapping("/save")
    List<Project> save(@RequestBody List<Project> projects);

}
