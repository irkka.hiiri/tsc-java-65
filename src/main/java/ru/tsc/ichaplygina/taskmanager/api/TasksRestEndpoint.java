package ru.tsc.ichaplygina.taskmanager.api;

import org.jetbrains.annotations.Nullable;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import java.util.List;

public interface TasksRestEndpoint {

    @Nullable
    @PostMapping("/add")
    List<Task> add(@RequestBody List<Task> tasks);

    @Nullable
    @GetMapping("/findAll")
    List<Task> findAll();

    @DeleteMapping("/remove")
    void remove(@RequestBody List<Task> tasks);

    @DeleteMapping("/removeAll")
    void removeAll();

    @Nullable
    @PostMapping("/save")
    List<Task> save(@RequestBody List<Task> tasks);

}
