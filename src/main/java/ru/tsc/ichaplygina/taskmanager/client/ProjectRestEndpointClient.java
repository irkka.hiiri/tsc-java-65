package ru.tsc.ichaplygina.taskmanager.client;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;
import ru.tsc.ichaplygina.taskmanager.api.ProjectRestEndpoint;
import ru.tsc.ichaplygina.taskmanager.model.Project;

public class ProjectRestEndpointClient implements ProjectRestEndpoint {

    private static final String BASE_URL = "http://localhost:8080/api/project/";

    @Override
    @Nullable
    public Project add(Project project) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = BASE_URL + "add";
        @NotNull final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        @NotNull final HttpEntity<Project> request = new HttpEntity<>(project, headers);
        return restTemplate.postForObject(url, request, Project.class);
    }

    @Override
    @Nullable
    public Project findById(String id) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = BASE_URL + "findById/{id}";
        return restTemplate.getForObject(url, Project.class, id);
    }

    @Override
    public void removeById(String id) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = BASE_URL + "removeById/{id}";
        restTemplate.delete(url, id);
    }

    @Override
    public void save(Project project) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = BASE_URL + "save";
        @NotNull final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        @NotNull final HttpEntity<Project> request = new HttpEntity<>(project, headers);
        restTemplate.put(url, request);
    }

}
