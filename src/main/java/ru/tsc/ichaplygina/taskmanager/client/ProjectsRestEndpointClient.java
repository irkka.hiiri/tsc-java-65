package ru.tsc.ichaplygina.taskmanager.client;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;
import ru.tsc.ichaplygina.taskmanager.api.ProjectsRestEndpoint;
import ru.tsc.ichaplygina.taskmanager.model.Project;

import java.util.Arrays;
import java.util.List;

public class ProjectsRestEndpointClient implements ProjectsRestEndpoint {

    private static final String BASE_URL = "http://localhost:8080/api/projects/";

    @Override
    @Nullable
    public List<Project> add(List<Project> projects) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = BASE_URL + "add";
        @NotNull final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        @NotNull final HttpEntity<List<Project>> request = new HttpEntity<>(projects, headers);
        return Arrays.asList(restTemplate.postForObject(url, request, Project[].class));
    }

    @Override
    @Nullable
    public List<Project> findAll() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = BASE_URL + "findAll";
        return Arrays.asList(restTemplate.getForObject(url, Project[].class));
    }

    @Override
    public void remove(List<Project> projects) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = BASE_URL + "remove";
        @NotNull final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        @NotNull final HttpEntity<List<Project>> request = new HttpEntity<>(projects, headers);
        restTemplate.postForObject(url, request, Project[].class);
    }

    @Override
    public void removeAll() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = BASE_URL + "removeAll";
        restTemplate.delete(url);
    }

    @Override
    @Nullable
    public List<Project> save(List<Project> projects) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = BASE_URL + "save";
        @NotNull final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        @NotNull final HttpEntity<List<Project>> request = new HttpEntity<>(projects, headers);
        return Arrays.asList(restTemplate.postForObject(url, request, Project[].class));
    }

}
