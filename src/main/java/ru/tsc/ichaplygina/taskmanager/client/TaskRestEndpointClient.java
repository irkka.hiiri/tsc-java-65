package ru.tsc.ichaplygina.taskmanager.client;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;
import ru.tsc.ichaplygina.taskmanager.api.TaskRestEndpoint;
import ru.tsc.ichaplygina.taskmanager.model.Task;

public class TaskRestEndpointClient implements TaskRestEndpoint {

    private static final String BASE_URL = "http://localhost:8080/api/task/";

    @Override
    @Nullable
    public Task add(Task task) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = BASE_URL + "add";
        @NotNull final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        @NotNull final HttpEntity request = new HttpEntity<>(task, headers);
        final Task result = restTemplate.postForObject(url, request, Task.class);
        return result;
    }

    @Override
    @Nullable
    public Task findById(String id) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = BASE_URL + "findById/{id}";
        return restTemplate.getForObject(url, Task.class, id);
    }

    @Override
    public void removeById(String id) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = BASE_URL + "removeById/{id}";
        restTemplate.delete(url, id);
    }

    @Override
    public void save(Task task) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = BASE_URL + "save";
        @NotNull final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        @NotNull final HttpEntity<Task> request = new HttpEntity<>(task, headers);
        restTemplate.put(url, request);
    }

}
