package ru.tsc.ichaplygina.taskmanager.client;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;
import ru.tsc.ichaplygina.taskmanager.api.TasksRestEndpoint;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import java.util.Arrays;
import java.util.List;

public class TasksRestEndpointClient implements TasksRestEndpoint {

    private static final String BASE_URL = "http://localhost:8080/api/tasks/";

    @Override
    @Nullable
    public List<Task> add(List<Task> tasks) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = BASE_URL + "add";
        @NotNull final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        @NotNull final HttpEntity<List<Task>> request = new HttpEntity<>(tasks, headers);
        return Arrays.asList(restTemplate.postForObject(url, request, Task[].class));
    }

    @Override
    @Nullable
    public List<Task> findAll() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = BASE_URL + "findAll";
        return Arrays.asList(restTemplate.getForObject(url, Task[].class));
    }

    @Override
    public void remove(List<Task> tasks) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = BASE_URL + "remove";
        @NotNull final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        @NotNull final HttpEntity<List<Task>> request = new HttpEntity<>(tasks, headers);
        restTemplate.postForObject(url, request, Task[].class);
    }

    @Override
    public void removeAll() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = BASE_URL + "removeAll";
        restTemplate.delete(url);
    }

    @Override
    @Nullable
    public List<Task> save(List<Task> tasks) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = BASE_URL + "save";
        @NotNull final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        @NotNull final HttpEntity<List<Task>> request = new HttpEntity<>(tasks, headers);
        return Arrays.asList(restTemplate.postForObject(url, request, Task[].class));
    }

}
