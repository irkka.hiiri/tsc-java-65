package ru.tsc.ichaplygina.taskmanager.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.tsc.ichaplygina.taskmanager.api.ProjectRestEndpoint;
import ru.tsc.ichaplygina.taskmanager.model.Project;
import ru.tsc.ichaplygina.taskmanager.repository.ProjectRepository;

@RestController
@RequestMapping("/api/project")
public class ProjectRestEndpointImpl implements ProjectRestEndpoint {

    @Autowired
    private ProjectRepository projectRepository;

    @Nullable
    @Override
    @PostMapping("/add")
    public Project add(@NotNull @RequestBody final Project project) {
        return projectRepository.write(project);
    }

    @Nullable
    @Override
    @GetMapping("/findById/{id}")
    public Project findById(@NotNull @PathVariable("id") final String id) {
        return projectRepository.findById(id);
    }

    @Override
    @DeleteMapping("/removeById/{id}")
    public void removeById(@NotNull @PathVariable("id") final String id) {
        projectRepository.removeById(id);
    }

    @Override
    @PutMapping("/save")
    public void save(@NotNull @RequestBody final Project project) {
        if (findById(project.getId()) != null) projectRepository.write(project);
    }
}
