package ru.tsc.ichaplygina.taskmanager.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.tsc.ichaplygina.taskmanager.api.ProjectsRestEndpoint;
import ru.tsc.ichaplygina.taskmanager.model.Project;
import ru.tsc.ichaplygina.taskmanager.repository.ProjectRepository;

import java.util.List;

@RestController
@RequestMapping("/api/projects")
public class ProjectsRestEndpointImpl implements ProjectsRestEndpoint {

    @Autowired
    private ProjectRepository projectRepository;

    @Override
    @Nullable
    @PostMapping("/add")
    public List<Project> add(@RequestBody @NotNull List<Project> projects) {
        return projectRepository.write(projects);
    }

    @Override
    @Nullable
    @GetMapping("/findAll")
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    @PostMapping("/remove")
    public void remove(@RequestBody @NotNull List<Project> projects) {
        projectRepository.remove(projects);
    }

    @Override
    @DeleteMapping("/removeAll")
    public void removeAll() {
        projectRepository.removeAll();
    }

    @Override
    @Nullable
    @PostMapping("/save")
    public List<Project> save(@RequestBody @NotNull List<Project> projects) {
        return projectRepository.write(projects);
    }
}
