package ru.tsc.ichaplygina.taskmanager.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.tsc.ichaplygina.taskmanager.api.TasksRestEndpoint;
import ru.tsc.ichaplygina.taskmanager.model.Task;
import ru.tsc.ichaplygina.taskmanager.repository.TaskRepository;

import java.util.List;

@RestController
@RequestMapping("/api/tasks")
public class TasksRestEndpointImpl implements TasksRestEndpoint {

    @Autowired
    private TaskRepository taskRepository;

    @Override
    @Nullable
    @PostMapping("/add")
    public List<Task> add(@RequestBody @NotNull List<Task> tasks) {
        return taskRepository.write(tasks);
    }

    @Override
    @Nullable
    @GetMapping("/findAll")
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    @PostMapping("/remove")
    public void remove(@RequestBody @NotNull List<Task> tasks) {
        taskRepository.remove(tasks);
    }

    @Override
    @DeleteMapping("/removeAll")
    public void removeAll() {
        taskRepository.removeAll();
    }

    @Override
    @Nullable
    @PostMapping("/save")
    public List<Task> save(@RequestBody @NotNull List<Task> tasks) {
        return taskRepository.write(tasks);
    }

}
