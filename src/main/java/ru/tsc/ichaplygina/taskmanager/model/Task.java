package ru.tsc.ichaplygina.taskmanager.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;
import ru.tsc.ichaplygina.taskmanager.enumerated.Status;

import java.sql.Timestamp;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class Task {

    @NotNull
    private String id = UUID.randomUUID().toString();

    @Nullable
    private String name;

    @Nullable
    private String description;

    @Nullable
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date created = new Date();

    @Nullable
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateFinish;

    @Nullable
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateStart;

    @NotNull
    private Status status = Status.PLANNED;

    @Nullable
    private String projectId;

    public Task(@NotNull final String name) {
        this.name = name;
    }

    public void setStatus(@NotNull final Status status) {
        this.status = status;
        if (status.equals(Status.COMPLETED)) dateFinish = new Timestamp(new Date().getTime());
        if (status.equals(Status.IN_PROGRESS)) dateStart = new Timestamp(new Date().getTime());
    }

}
