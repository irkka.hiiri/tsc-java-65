package ru.tsc.ichaplygina.taskmanager;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.ichaplygina.taskmanager.client.ProjectRestEndpointClient;
import ru.tsc.ichaplygina.taskmanager.client.ProjectsRestEndpointClient;
import ru.tsc.ichaplygina.taskmanager.marker.IntegrationCategory;
import ru.tsc.ichaplygina.taskmanager.model.Project;

public class ProjectRestEndpointTest {

    @NotNull
    final ProjectRestEndpointClient client = new ProjectRestEndpointClient();

    @NotNull
    final Project project1 = new Project("Test Project 1");

    @NotNull
    final Project project2 = new Project("Test Project 2");

    @NotNull
    final Project project3 = new Project("Test Project 3");

    @NotNull
    final Project project4 = new Project("Test Project 4");

    @Before
    public void initTest() {
        client.add(project1);
        client.add(project2);
        client.add(project3);
    }

    @After
    public void clean() {
        @NotNull final ProjectsRestEndpointClient client = new ProjectsRestEndpointClient();
        client.removeAll();
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testAdd() {
        @NotNull final String expected = project4.getName();
        @Nullable final Project project = client.add(project4);
        Assert.assertNotNull(project);
        @NotNull final String actual = project.getName();
        Assert.assertEquals(expected, actual);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void findById() {
        @NotNull final String expected = project1.getId();
        @Nullable final Project project = client.findById(expected);
        Assert.assertNotNull(project);
        final String actual = project.getId();
        Assert.assertEquals(expected, actual);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void removeById() {
        @NotNull final String id = project1.getId();
        client.removeById(id);
        Assert.assertNull(client.findById(id));
    }

    @Test
    @Category(IntegrationCategory.class)
    public void save() {
        client.save(project4);
        Assert.assertNull(client.findById(project4.getId()));
        @NotNull final String expected = "Test Project One";
        project1.setName(expected);
        client.save(project1);
        @NotNull final String actual = client.findById(project1.getId()).getName();
        Assert.assertEquals(expected, actual);
    }

}
