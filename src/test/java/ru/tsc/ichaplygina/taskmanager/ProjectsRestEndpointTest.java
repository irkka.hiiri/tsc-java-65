package ru.tsc.ichaplygina.taskmanager;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.ichaplygina.taskmanager.client.ProjectRestEndpointClient;
import ru.tsc.ichaplygina.taskmanager.client.ProjectsRestEndpointClient;
import ru.tsc.ichaplygina.taskmanager.marker.IntegrationCategory;
import ru.tsc.ichaplygina.taskmanager.model.Project;

import java.util.Arrays;
import java.util.List;

public class ProjectsRestEndpointTest {

    @NotNull
    final ProjectsRestEndpointClient client = new ProjectsRestEndpointClient();

    @NotNull
    final Project project1 = new Project("Test Project 1");

    @NotNull
    final Project project2 = new Project("Test Project 2");

    @NotNull
    final Project project3 = new Project("Test Project 3");

    @NotNull
    final Project project4 = new Project("Test Project 4");

    @NotNull
    final Project project5 = new Project("Test Project 5");

    @NotNull
    final Project project6 = new Project("Test Project 6");

    @NotNull
    final List<Project> projectList1 = Arrays.asList(project1, project2, project3);

    @NotNull
    final List<Project> projectList2 = Arrays.asList(project4, project5, project6);

    @Before
    public void initTest() {
        client.add(projectList1);
    }

    @After
    public void clean() {
        client.removeAll();
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testAdd() {
        @NotNull final List<Project> projects = client.add(projectList2);
        @NotNull final ProjectRestEndpointClient projectClient = new ProjectRestEndpointClient();
        Assert.assertNotNull(projectClient.findById(project6.getId()));
    }

    @Test
    @Category(IntegrationCategory.class)
    public void findAll() {
        @Nullable final List<Project> projects = client.findAll();
        Assert.assertEquals(3, projects.size());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void remove() {
        client.remove(projectList1);
        Assert.assertTrue(client.findAll().size() == 0);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void removeAll() {
        client.removeAll();
        Assert.assertTrue(client.findAll().size() == 0);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void save() {
        projectList1.get(0).setName("Changed Name");
        @NotNull final List<Project> projects = client.save(projectList1);
        Assert.assertEquals("Changed Name", projects.get(0).getName());
    }

}
