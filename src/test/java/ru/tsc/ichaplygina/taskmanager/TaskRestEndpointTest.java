package ru.tsc.ichaplygina.taskmanager;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.ichaplygina.taskmanager.client.TaskRestEndpointClient;
import ru.tsc.ichaplygina.taskmanager.client.TasksRestEndpointClient;
import ru.tsc.ichaplygina.taskmanager.marker.IntegrationCategory;
import ru.tsc.ichaplygina.taskmanager.model.Task;

public class TaskRestEndpointTest {

    @NotNull
    final TaskRestEndpointClient client = new TaskRestEndpointClient();

    @NotNull
    final Task task1 = new Task("Test Task 1");

    @NotNull
    final Task task2 = new Task("Test Task 2");

    @NotNull
    final Task task3 = new Task("Test Task 3");

    @NotNull
    final Task task4 = new Task("Test Task 4");

    @Before
    public void initTest() {
        client.add(task1);
        client.add(task2);
        client.add(task3);
    }

    @After
    public void clean() {
        @NotNull final TasksRestEndpointClient client = new TasksRestEndpointClient();
        client.removeAll();
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testAdd() {
        @NotNull final String id = task4.getId();
        System.out.println(id);
        System.out.println(task4.getName());
        client.add(task4);
        @Nullable final Task task = client.findById(id);
        Assert.assertNotNull(task);
        Assert.assertEquals(task4.getName(), task.getName());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void findById() {
        @NotNull final String expected = task1.getId();
        @Nullable final Task task = client.findById(expected);
        Assert.assertNotNull(task);
        final String actual = task.getId();
        Assert.assertEquals(expected, actual);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void removeById() {
        @NotNull final String id = task1.getId();
        client.removeById(id);
        Assert.assertNull(client.findById(id));
    }

    @Test
    @Category(IntegrationCategory.class)
    public void save() {
        client.save(task4);
        Assert.assertNull(client.findById(task4.getId()));
        @NotNull final String expected = "Test Project One";
        task1.setName(expected);
        client.save(task1);
        @NotNull final String actual = client.findById(task1.getId()).getName();
        Assert.assertEquals(expected, actual);
    }

}
