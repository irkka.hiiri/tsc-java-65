package ru.tsc.ichaplygina.taskmanager;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.ichaplygina.taskmanager.client.TaskRestEndpointClient;
import ru.tsc.ichaplygina.taskmanager.client.TasksRestEndpointClient;
import ru.tsc.ichaplygina.taskmanager.marker.IntegrationCategory;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import java.util.Arrays;
import java.util.List;

public class TasksRestEndpointTest {

    @NotNull
    final TasksRestEndpointClient client = new TasksRestEndpointClient();

    @NotNull
    final Task task1 = new Task("Test Task 1");

    @NotNull
    final Task task2 = new Task("Test Task 2");

    @NotNull
    final Task task3 = new Task("Test Task 3");

    @NotNull
    final Task task4 = new Task("Test Task 4");

    @NotNull
    final Task task5 = new Task("Test Task 5");

    @NotNull
    final Task task6 = new Task("Test Task 6");

    @NotNull
    final List<Task> taskList1 = Arrays.asList(task1, task2, task3);

    @NotNull
    final List<Task> taskList2 = Arrays.asList(task4, task5, task6);

    @Before
    public void initTest() {
        client.add(taskList1);
    }

    @After
    public void clean() {
        client.removeAll();
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testAdd() {
        @NotNull final List<Task> tasks = client.add(taskList2);
        @NotNull final TaskRestEndpointClient taskClient = new TaskRestEndpointClient();
        Assert.assertNotNull(taskClient.findById(task6.getId()));
    }

    @Test
    @Category(IntegrationCategory.class)
    public void findAll() {
        @Nullable final List<Task> tasks = client.findAll();
        Assert.assertEquals(3, tasks.size());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void remove() {
        client.remove(taskList1);
        Assert.assertTrue(client.findAll().size() == 0);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void removeAll() {
        client.removeAll();
        Assert.assertTrue(client.findAll().size() == 0);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void save() {
        taskList1.get(0).setName("Changed Name");
        @NotNull final List<Task> tasks = client.save(taskList1);
        Assert.assertEquals("Changed Name", tasks.get(0).getName());
    }

}
